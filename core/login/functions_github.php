<?php
/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//Version 1.0 - Cyril CARGOU - TiCoop - Login function to OIDC Github connection

/**
 *      \file       htdocs/core/login/functions_github.php
 *      \ingroup    core
 *      \brief      Authentication functions for GitHub mode
 */

//include_once DOL_DOCUMENT_ROOT.'/core/class/openid.class.php';
include_once DOL_DOCUMENT_ROOT.'/user/class/user.class.php';

/**
 * Check validity of user/password/entity
 * If test is ko, reason must be filled into $_SESSION["dol_loginmesg"]
 *
 * @param	string	$usertotest		Token GUID, not a user login !
 * @param	string	$passwordtotest	not needed, kept for compatibility
 * @param   int		$entitytotest   not needed, kept for compatibility
 * @return	string					Login if OK, '' if KO
 */
function check_user_password_github($usertotest='', $passwordtotest='', $entitytotest='')
{
    //global $db,$conf,$langs,$user;

    dol_syslog("functions_github::check_user_password_github ");

    $login='';
    if( isset($_SESSION["github_login"]))
    {
        dol_syslog('Login sucess for login='.$login);
        $login = $_SESSION["github_login"];
    }
    else
    {
        dol_syslog('Login error for login='.$login);
    }

    return $login;
}
