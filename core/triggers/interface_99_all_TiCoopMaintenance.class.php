<?php
/* 
  Auteur : Maxime Collin pour TiCoop commission Informatique
  Date : 2021/10/21
  Description : Trigger de déclanchement du message de maintenance lors de la connexion à son compte

  v1.0 | 2021-10-21 | Maxime Collin | Création du document

  Ce trigger va regarder au moment de la connexion des utilisateurs si le message de maintenance est défini
  Si oui, il affiche le message.

  Le message de maintenance est défini dans le fichier /var/www/html/langs/xx_XX/TiCoopMaintenance.lang
  Où xx_XX est la langue
 */

/**
 *  \file       htdocs/core/triggers/interface_0_TicoopMemberEvents.class.php
 *  \ingroup    TiCoop core
 *  \brief      Trigger file for TiCoop user process creation
 */

require_once DOL_DOCUMENT_ROOT.'/core/triggers/dolibarrtriggers.class.php';


/**
 *  Class of triggers for security audit events
 */
class InterfaceTiCoopMaintenance extends DolibarrTriggers
{
	/**
	 * @var string Image of the trigger
	 */
	public $picto = 'technic';

	public $family = 'core';

	public $description = "Triggers of this module allows to display maintenance message on login.";

	/**
	 * Version of the trigger
	 * @var string
	 */
	public $version = self::VERSION_DOLIBARR;
    

	/**
	 * Function called when a Dolibarrr security audit event is done.
	 * All functions "runTrigger" are triggered if file is inside directory htdocs/core/triggers or htdocs/module/code/triggers (and declared)
	 *
	 * @param string		$action		Event action code
	 * @param Object		$object     Object
	 * @param User			$user       Object user
	 * @param Translate		$langs      Object langs
	 * @param conf			$conf       Object conf
	 * @return int         				<0 if KO, 0 if no triggered ran, >0 if OK
	 */
	public function runTrigger($action, $object, User $user, Translate $langs, Conf $conf)
    {
        global $db;
        // Actions
        //https://wiki.dolibarr.org/index.php/Triggers-actions
        switch($action)
        {
            case 'USER_LOGIN':
                try
                { 
                    //si event créé par lui même arret (pour éviter les boucles sans fin du fait que l'event peut modifier un objet)
                    $stack = debug_backtrace();
                    array_shift ( $stack );//supprime la 1er entrée (la notre)
                    $result = array_search ( __FILE__ , array_column($stack, 'file'), true );
                    if($result != $false) return 1;

                    //$date = dol_now();
                    dol_syslog("TiCoop - maintenance : Vérification si le message de maintenance est défini ou non");

                    // Load lang file
                    $langs->load('TiCoopMaintenance');

                    // Si le message de maintenance est défini (trans() retourne la clé s'il ne trouve pas une correspondance clé = message)
                    if ($langs->trans("maintenance") != "maintenance")
                    {
                        // On l'envoi
                        dol_syslog("TiCoop - maintenance : Message défini, affichage");
                        setEventMessage($langs->trans("maintenance"), 'warnings');
                    }
                    // Sinon on ne fait rien
                }
                catch (Exception $e) 
                {
                    dol_syslog("TiCoop - Erreur : ".$e->getMessage());
                    setEventMessage("TiCoop - Erreur : ".$e->getMessage(), 'errors');
                    return 1;
                }
                break; //pour la forme :-)
            default://do nothing
        }

        return 0;
    }

}

