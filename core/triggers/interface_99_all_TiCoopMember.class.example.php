<?php
/* 
  Auteur : Cyril CARGOU pour TiCoop commission Informatique
  Date : 2019/09/22
  Description : Trigger de déclanchement du processus de création de nouveau utilisateurs chez TiCoop

  v1.0 | 2019-09-22 | Cyril CARGOU | Création du document pour interfacer l'ERP à l'IEM KeyCloak
  v2.0 | 2019-09-22 | Cyril CARGOU | Création du document pour interfacer l'ERP à l'outil de mailling SendInBlue
  v2.1 | 2019-01-15 | Cyril CARGOU | Correction d'un bug de suppression des accents
  v2.2 | 2020-03-20 | Cyril CARGOU | Correction d'un bug d'import de httpful.phar
  v2.3 | 2020-05-23 | Cyril CARGOU | Correction d'un bug de gestion du login utilisateur
  v2.4 | 2021-04-26 | Maxime COLLIN | Prise en compte du statut "Non-coopérateur"
  V3.0 | 2022-01-21 | Maxime Collin | Création de variables pour génériser la classe (keycloak base url, user, passwd + Sendinblue apikey)
 */

// ATTENTION ! Les variables :
// - KEYCLOAK_BASE_URL
// - KEYCLOAK_REALM_NAME
// - KEYCLOAK_USER
// - KEYCLOAK_PASSWD
// - SENDINBLUE_APIKEY
// doivent être changées

/**
 *  \file       htdocs/core/triggers/interface_0_TicoopMemberEvents.class.php
 *  \ingroup    TiCoop core
 *  \brief      Trigger file for TiCoop user process creation
 */

require_once DOL_DOCUMENT_ROOT.'/core/triggers/dolibarrtriggers.class.php';


/**
 *  Class of triggers for security audit events
 */
class InterfaceTiCoopMember extends DolibarrTriggers
{
	/**
	 * @var string Image of the trigger
	 */
	public $picto = 'technic';

	public $family = 'core';

	public $description = "Triggers of this module allows to run TiCoop user process creation.";

    private $KEYCLOAK_BASE_URL = 'https://your.keycloak.domain';
    private $KEYCLOAK_REALM_NAME = 'YOUR_REAL_NAME';
    private $KEYCLOAK_USER = 'YOUR.USERNAME';
    private $KEYCLOAK_PASSWD = 'YOUR.PASSWD';

    private $SENDINBLUE_APIKEY = 'YOUR_SENDINBLUE_APIKEY';

	/**
	 * Version of the trigger
	 * @var string
	 */
	public $version = self::VERSION_DOLIBARR;
    
    private $token=null;
    
    /**
    * liste des types d'adhérents qui ouvre la création de compte dans keycloak
    */
    private $array_typeAccount = array("Cooperateur A", "Non-coopérateur");
    

	/**
	 * Function called when a Dolibarrr security audit event is done.
	 * All functions "runTrigger" are triggered if file is inside directory htdocs/core/triggers or htdocs/module/code/triggers (and declared)
	 *
	 * @param string		$action		Event action code
	 * @param Object		$object     Object
	 * @param User			$user       Object user
	 * @param Translate		$langs      Object langs
	 * @param conf			$conf       Object conf
	 * @return int         				<0 if KO, 0 if no triggered ran, >0 if OK
	 */
	public function runTrigger($action, $object, User $user, Translate $langs, Conf $conf)
    {
        global $db;
        // Actions
        //https://wiki.dolibarr.org/index.php/Triggers-actions
        //ici $objet est de class Adherent -> https://github.com/Dolibarr/dolibarr/blob/develop/htdocs/adherents/class/adherent.class.php
        switch($action)
        {
            case 'MEMBER_VALIDATE':
            case 'MEMBER_MODIFY':
            case 'MEMBER_DELETE':
            case 'MEMBER_RESILIATE':
                try
                { 
                    //TiCoop import
                    //Import que quand cela est necessire (eviter des pb pour d'autres events)
                    require_once DOL_DOCUMENT_ROOT.'/core/triggers/httpful.phar';
                    //si event créé par lui même arret (pour éviter les boucles sans fin du fait que l'event peut modifier un objet)
                    $stack = debug_backtrace();
                    array_shift ( $stack );//supprime la 1er entrée (la notre)
                    $result = array_search ( __FILE__ , array_column($stack, 'file'), true );
                    if($result != $false) return 1;

                    //$date = dol_now();
                    dol_syslog("TiCoop Trigger '".$this->name."' for action '$action' launched by ".__FILE__.". id=".$object->id);

                    dol_syslog("TiCoop - Object class :".get_class($object));
                    dol_syslog("TiCoop - firstname :".$object->firstname);
                    dol_syslog("TiCoop - lastname :".$object->lastname);
                    dol_syslog("TiCoop - morphy :".$object->morphy);
                    dol_syslog("TiCoop - email :".$object->email);
                    dol_syslog("TiCoop - photo :".$object->photo);
                    dol_syslog("TiCoop - type :".$object->type);
                    dol_syslog("TiCoop - SMS :".$object->phone_mobile);
                    dol_syslog("TiCoop - newsletter :".$object->array_options["options_newsletter"]);
                    //dol_syslog("TiCoop - idKeyCloak :".$object->array_options["options_idKeyCloak"]);//le login est utilisé

                    //Test si l'utilisateur est physique et d'un type qui ouvre la création de compte, sinon pas besoin de compte
                    if($object->morphy != 'phy' || !in_array($object->type, $this->array_typeAccount))
                    {
                        dol_syslog("TiCoop - Utilisateur non physique ou d'une catégorie n'ouvrant pas de compte, pas d'action");
                        return 1;
                    }
                    dol_syslog("TiCoop - Utilisateur physique et de type ouvrant un compte, poursuite");

                    //par défaut le login n'est pas chargé dans l'objet quand l'option "Gérer un identifiant pour chaque adhérent" est à non
                    dol_syslog("TiCoop - Récupératon du login à partire de l'ID :".$object->id);

                    $sql = "Select login from llx_adherent where rowid=".$object->id;
                    $resql = $this->db->query($sql);
                    if ($resql) {
                        if ($this->db->num_rows($resql)) {
                            $obj = $this->db->fetch_object($resql);
                            dol_syslog("TiCoop - login :".$obj->login);//set by keaycloak return
                        }
                    }

                    $aduser = new Adherent($db);
                    $intResult = $aduser->fetch($object->id);#>0 if OK, 0 if not found, <0 if KO
                    if ($intResult <= 0)
                    {
                        dol_syslog("TiCoop - ERREUR : Compte non trouvé dans l'ERP, code :".$intResult);
                        setEventMessage("TiCoop - ERREUR de création du compte KeyCloack et Sendinblue", 'errors');
                        return 1;
                    }
                    dol_syslog("TiCoop - Compte ERP trouvé :".$aduser->firstname." ".$aduser->lastname);
                    $object->login = $aduser->login;
                    dol_syslog("TiCoop - login :".$object->login);//set by keaycloak return
                    
                    //Vérifie que le login exsite bien dans KeyCloak
                    if(isset($object->login) && strlen($object->login) > 2)
                    {
                        #le champs 
                        dol_syslog("TiCoop - Login utilisateur trouvé, verification du lien avec keycloak");
                        $kcUser = $this->keyCloak_searchUserByLogin($object->login);
                        if($kcUser != null)
                        {
                            dol_syslog("TiCoop - Compte trouvé");
                        }
                        else
                        {
                            dol_syslog("TiCoop - Compte non trouvé, creation du compte");
                            dol_syslog("TiCoop - Creation du compte KeyCloak ...");
                            $kcUser = $this->keyCloak_creatUser($object->firstname,$object->lastname,$object->email);
                            dol_syslog("TiCoop - Fait :".$kcUser->username);
                        }
                    }
                    else
                    {
                        dol_syslog("TiCoop - Compte Dolibarr sans lien avec KeyCloak, recherche d'un utilisateur similaire ...");
                        $kcUser = $this->keyCloak_searchUserByEmail($object->email);
                        
                        if($kcUser != null)
                        {
                            //utilisateur trouvé
                            dol_syslog("TiCoop - Compte KeyCloak trouve :".$kcUser->username);
                        }
                        else //création du compte (dans tous les cas, même pour l'action ERP de "suppression")
                        {
                            dol_syslog("TiCoop - Compte KeyCloak non trouve");
                            //Création de l'utilisateur
                            dol_syslog("TiCoop - Creation du compte KeyCloak ...");
                            $kcUser = $this->keyCloak_creatUser($object->firstname,$object->lastname,$object->email);
                            dol_syslog("TiCoop - Fait :".$kcUser->username);
                            setEventMessage("TiCoop - Creation du compte KeyCloak faite");
                        }
                        //Mise  à jour du champs login
                        $object->login = $kcUser->username;
                        //Mise à jour de l'utilisateur
                        dol_syslog("TiCoop - Mise à jour de compte Dolibarr avec le login ...");
                        $object->update($user,1);//1=disable trigger UPDATE

                        //getion d'erreur
                        if(!isset($object->login) || $object->login == null)
                            throw new Exception("Erreur, compte KeyCloak non disponible.");
                    }

                    if($action=='MEMBER_DELETE' || $action=='MEMBER_RESILIATE')
                    {
                        dol_syslog("TiCoop - Desactivation du compte KeyCloak...");
                        $this->keyCloak_disableUserByLogin($object->login);
                        setEventMessage("TiCoop - Desactivation du compte et KeyCloak faite");
                    }  
                    if($action=='MEMBER_VALIDATE')
                    {
                        dol_syslog("TiCoop - Reactivation du compte et KeyCloak...");
                        $this->keyCloak_enableUserByLogin($object->login);
                        setEventMessage("TiCoop - Reactivation du compte KeyCloak faite");
                    }
                    else
                    {
                        dol_syslog("TiCoop - Mise à jour du compte KeyCloak : ".$object->login);
                        $this->keyCloak_updateUser($object->login,$object->firstname,$object->lastname,$object->email);
                        setEventMessage("TiCoop - Mise à jour du compte KeyCloak faite");
                    }

                    //Ti Coop 2020-01-12 :  Ajout de la gestion de SendinBlue
                    //Mise à jour du compte à partire des infomrations de l'ERP
                    //et, dans le cas d'une suppression, force le blocage du compte
                    dol_syslog("TiCoop - Interface avec l'annuaire Sendinblue");
                    if($action=='MEMBER_DELETE' || $action=='MEMBER_RESILIATE') 
                    {
                        $blacklistUser = true;
                        dol_syslog("TiCoop - Désactivation du compte (blocage forcé)");
                    }
                    else
                    {
                        //oui si l'utilisateur accepte de recevoir une newsletter
                        if($object->array_options["options_newsletter"] == "oui")
                        {
                            //$blacklistUser = false; //l'action de débloquer un compte n'est possible que si l'utilisateur ne s'est pas désincrit volontairement.
                            //si l'utilisateur s'est désincrit manuellement de la newsletter, conservation de ce choi.
                            dol_syslog("TiCoop - Detection si l'administrateur peut debloquer le contact");
                            // inversion de résulat car faux = pas de blocage.

                            if($this->Sendinblue_canUnblacklistUser($object->email))
                            {
                                dol_syslog("TiCoop - L'administrateur peut debloquer le contact");
                                $blacklistUser = false;
                            }
                            else
                            {
                                dol_syslog("TiCoop - L'administrateur ne peut pas debloquer le contact");
                                $blacklistUser = false;
                            }
                        }
                        else
                        {
                            $blacklistUser = true;//l'action de bloquer est toujours possible
                        }
                    }
                    //action
                    $this->Sendinblue_interfaceContact($object->firstname,
                                                        $object->lastname,
                                                        $object->email,
                                                        $object->phone_mobile,
                                                        $blacklistUser);
                    setEventMessage("TiCoop - Contact Sendinblue à jour");

                    dol_syslog("TiCoop - Fait");
                    return 1;
                }
                catch (Exception $e) 
                {
                    dol_syslog("TiCoop - Erreur : ".$e->getMessage());
                    setEventMessage("TiCoop - Erreur : ".$e->getMessage(), 'errors');
                    //return -1; //affiche une erreur mais ne bloque pas.
                    return 1;
                }
                break; //pour la forme :-)
            default://do nothing
        }

        return 0;
    }

    private function keyCloak_searchUserByLogin($login)
    {
        return $this->keyCloak_searchUser(array("username" => $login));
    }
    private function keyCloak_searchUserByEmail($email)
    {
        return $this->keyCloak_searchUser(array("email" => $email));
    }
    private function keyCloak_searchUser($searchArray)
    {
        //search user by email
        $token = $this->getToken();
        $data = $this->APIKeyCloak_send($token,$this->APIKeyCloak_userURI(),$searchArray);
        //si pas trouvé le compte
        if(count($data) == 0) 
        {
            return null;
        }
        //compte trouvé
        return $data[0];
       /* echo "firstName : ".$data[0]->firstName."\n";
        echo "lastName : ".$data[0]->lastName."\n";
        echo "username : ".$data[0]->username."\n";
        echo "email : ".$data[0]->email."\n";
        echo "id : ".$data[0]->id."\n";*/
    }

    private function keyCloak_creatUser($firstname,$lastname,$email)
    {
        if($firstname == null ||$lastname == null || $email == null)
        {
            throw new Exception("Nom, prenom ou mail vide. Stop.");
        }
        
        //---------- Action 0 - Vérification si le email n'exsite pas déja
        $kcUser = $this->keyCloak_searchUserByEmail($email); 
        if($kcUser != null) throw new Exception("Utilisateur déja exsitant dans KeyCloak avec cette adress e-mail : ".$email);
        //---------- Action 0 - Vérification si le login n'exsite pas déja
        //Création login
        $login = $this->createLogin($firstname,$lastname);
        $watchDog = 3;
        //fait trois tantatives de création du compte
        $sufix = '';
        for($i=1;;$i++)//sortie en erreur
        {
            $sufix = $i;
            if($i==1) $sufix = '';//pas de suffix pour le 1er compte :-)
            $kcUser = $this->keyCloak_searchUserByLogin($login.$sufix); 
            if($kcUser == null)  break;//si on ne trouve pas l'utilisateur, fin de la création du login
            if($i >= $watchDog) throw new Exception("Des utilisateurs existent déja avec ce login : ".$login);
        }
        $login .= $sufix;
        //---------- Action 1 - Creation d'un utilisateur
        //POST /{realm}/users
        $action = array(
            "username"  => strtolower($login),
            "enabled" => "true",
            "emailVerified"  => "true",
            "firstName" => ucfirst(strtolower($firstname)),
            "lastName" => strtoupper($lastname),
            "email" => strtolower($email)
            );
        //ne retourn rien
        $token = $this->getToken();
        $this->APIKeyCloak_send($token,$this->APIKeyCloak_userURI(),$action,"POST");
        
        //---------- Action 2 - Récupération de l'utilisateur créé
        $kcUser = $this->keyCloak_searchUserByLogin($login);
        if($kcUser == null) throw new Exception("Erreur création du compte impossible"); 
        
        //---------- Action 3 - Envoi du mail pour se connecter et avoir le mdp
        //Send veryify email, ne retourne rien
        //$this->APIKeyUserCloak_send($token,"/auth/admin/realms/ticoop/users/".$kcUser->id."/send-verify-email",null,"PUT");
        $uri = $this->APIKeyCloak_userURI() . "/" . $kcUser->id . "/execute-actions-email";
        $this->APIKeyCloak_send($token,$uri,Array('UPDATE_PASSWORD'),'PUT');

        return $kcUser;
    }

    private function keyCloak_enableUserByLogin($login)
    {
        $action = array(
            "username"  => $login,
            "enabled" => "true",
            );
        return $this->keyCloak_updateUsers($action);
    }

    private function keyCloak_disableUserByLogin($login)
    {
        $action = array(
            "username"  => $login,
            "enabled" => "false",
            );
        return $this->keyCloak_updateUsers($action);
    }

    private function keyCloak_updateUser($login,$firstname,$lastname,$email)
    {
        $action = array(
            "username"  => strtolower($login),
            "emailVerified"  => "true",
            "firstName" => ucfirst(strtolower($firstname)),
            "lastName" => strtoupper($lastname),
            "email" => strtolower($email)
            );
        return $this->keyCloak_updateUsers($action);
    }

    private function keyCloak_updateUsers($array)
    {
        $login = $array['username'];
        if($login == null) throw new Exception("Login utilisateur null");
    
        //---------- Action 1 - Récupération de l'utilisateur
        $kcUser = $this->keyCloak_searchUserByLogin($login);  
        if($kcUser == null) throw new Exception("Mise à jour impossible, Utilisateur non trouvé dans KeyCloak : ".$login); 
         //---------- Action 2 - Mise à jour de l'utilisateur
        /*$array = array(
            "username"  => $login,
            //"enabled" => "true", ne pas l'activer car il est possible de modifier un utiliser résilié
            "emailVerified"  => "true",
            "firstName" => $firstname,
            "lastName" => strtoupper($lastname),
            "email" => $email
            );*/
        $token = $this->getToken();
        $uri = $this->APIKeyCloak_userURI() . "/" . $kcUser->id; 
        $this->APIKeyCloak_send($token,$uri,$array,"PUT");
        return null;
    }

    private function getToken()
    {
        if( $this->token == null)
        {
            //TODO passer par variables dont le mdp n'est pas visible ...
            $uri = "/auth/realms/".$this->KEYCLOAK_REALM_NAME."/protocol/openid-connect/token";
            $login = $this->KEYCLOAK_USER;
            $mdp = $this->KEYCLOAK_PASSWD;
            if( $mdp == "") throw new Exception("Mot de passe du compte de service Keycloak vide");
            $this->token = $this->APIKeyCloak_getToken($uri,$login,$mdp);
        }
        if( $this->token == null) throw new Exception("Token vide, recuperation echouee");
        return  $this->token;
    }

    private function APIKeyCloak_getToken($uri,$login,$pwd)
    {
        $url = $this->KEYCLOAK_BASE_URL . $uri;
        #https://github.com/keycloak/keycloak-documentation/blob/master/server_development/topics/admin-rest-api.adoc
        $data = array(
            "client_id"  => "admin-cli",
            "username" => $login,
            "password" => $pwd,
            "grant_type" => "password"
            );  
        $data=http_build_query($data);
        $response = \Httpful\Request::post($url)
                        ->addHeader('Accept', 'application/json')
                        ->addHeader('Content-Type', 'application/x-www-form-urlencoded')
                        ->body($data)
                        ->send();
        if($response->meta_data['http_code'] >= 300) throw new Exception("Error : ".$response->meta_data['http_code']);
        return $response->body->access_token;
    }

    private function APIKeyCloak_send($token,$uri,$data=null,$methode="GET")
    {
        $url = $this->KEYCLOAK_BASE_URL . $uri;
        if(!isset($token) || $token == null) throw new Exception("Token null");
        return $this->APIREST_send(array("Authorization",'Bearer '. $token),$url,$data,$methode);
    }

    private function APIKeyCloak_userURI()
    {
        return "/auth/admin/realms/" . $this->KEYCLOAK_REALM_NAME . "/users";
    }

    private function createLogin($firstname,$lastname)
    {
        if(!isset($firstname) || $firstname == null ) throw new Exception("Prenom obligatoire");
        if(!isset($lastname) || $lastname == null ) throw new Exception("Nom obligatoire");
        return $this->cleanLogin(strtolower($firstname.'.'.$lastname));
    }

    private function cleanLogin( $str, $charset='utf-8' ) 
    {
 
        //supprime accents
        $str = htmlentities( $str, ENT_NOQUOTES, $charset );
        
        $str = preg_replace( '#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str );
        $str = preg_replace( '#&([A-za-z]{2})(?:lig);#', '\1', $str );
        $str = preg_replace( '#&[^;]+;#', '', $str );
        
        //supprime caratere spécieux et espace
        $vowels = array("'","`","\"", "{", "}", "\\", "/", "(", ")", "|", "@", "_", " ");
        $str = str_replace($vowels, "", $str);
        return $str;
    }

    //Fonction de base pour l'interface avec Seninblue
    private function APISendinblue_send($url,$data=null,$methode="GET")
    {
        return $this->APIREST_send(array("api-key",$this->SENDINBLUE_APIKEY),$url,$data,$methode);
    }

    //Fonction de récupration d'un contact, si exsitant
    private function Sendinblue_getContact($email)
    {
        try
        {
            $out = $this->APISendinblue_send("https://api.sendinblue.com/v3/contacts/$email");
            //si le contact n'exite pas
            if(isset($out->code) && $out->code == "document_not_found")
                return null;
            return $out;
        }
        catch (Exception $e)
        {
            return null; //le contact n'existe pas. 
        }
    }

    //Fonction d'identification si le compte est bloqué par une action admin (ici l'erp) ou l'utilisateur
    //note si le contact est déja débloqué, retour oui
    private function Sendinblue_canUnblacklistUser($email)
    {
        /* "unsubscriptions":
        {"userUnsubscription":[],
        "adminUnsubscription":[{"eventTime":"2020-01-12T21:51:19.885+01:00","ip":"46.105.103.178"},
        {"eventTime":"2020-01-13T23:04:41.372+01:00","ip":"46.105.103.178"}]},*/
        $contact = $this->Sendinblue_getContact($email);
        if($contact === null) 
            return null;

        //si l'utilisteur n'est pas bloqué, retour true, le délbocage est possible car sans effet :-)
        if(!$contact->emailBlacklisted)
            return true;

        //si l'utilisateur n'a pas déja subit aucune desincription / blocage, c'est simple, l'ERP est maitre de l'information
        if(!isset($contact->statistics->unsubscriptions))
            return true;
        //l'utilisateur a été bloqué, mais par qui ?
        //userUnsubscription < 0, pas par lui même => donc par l'erp ou l'admin sendinblue => il est possibile de le réactiver
        if(count($contact->statistics->unsubscriptions->userUnsubscription) <= 0)
            return true;
        //adminUnsubscription < 0, l'utilisateur s'est désincrit lui même, ne pas le réactiver depuis l'ERP ! (passer par sendinblue sur demande de l'utilisateur)
        if(count($contact->statistics->unsubscriptions->adminUnsubscription) <= 0)
            return false;
        //si nous arrvions à ce point c'est que les deux types de désincription exsitent, identification de la derniere date de chacun
        $dateUserUnsubscription = $contact->statistics->unsubscriptions->userUnsubscription[count($contact->statistics->unsubscriptions->userUnsubscription)-1]->eventTime;
        $dateAdminUnsubscription = $contact->statistics->unsubscriptions->adminUnsubscription[count($contact->statistics->unsubscriptions->adminUnsubscription)-1]->eventTime;
        //comme les date sont en mode anglais une comparaison des deux chains permet d'indiquer quelle est la date la plus récente
        //si strcmp  retourne -1 alors dateUserUnsubscription est plus anciene que dateAdminUnsubscription, donc que l'admin à repris la main
        //si strcmp retour 1 alors dateUserUnsubscription est plus récente que dateAdminUnsubscription, donc que l'utiisateur à repris la main
        //strcmp ne peux pas retour 0, la date et le temps sont au centieme de seconde :-)
        $out = strcmp ($dateUserUnsubscription,$dateAdminUnsubscription);
        if($out == -1)//le dernier blocage a été fait par l'administrateur, nous pouvons donc le débloquer
            return true;
        return false;
    }

    //Fonction unique de création/gestion d'un contact Sendinblue
    private function Sendinblue_interfaceContact($firstname,$lastname,$email,$phone_mobile,$blockage,$idList = array(3))
    {
        //Attention le mail et le sms doivent être unique!

        //configuration en mode international, france par défaut
        //06XXXXXXXX => 336XXXXXXXX
        $digit = substr($phone_mobile,0,2);
        if($digit == "06" || $digit == "07")
        {
            $phone_mobile = "33" . substr($phone_mobile,1);
        }
        $data = array(
            "email"  => $email,
            "emailBlacklisted" => $blockage,
            "smsBlacklisted" => $blockage,
            "listIds" => $idList,
            "updateEnabled" => true,
            "attributes" => array(
                        "NOM" => $lastname,
                        "PRENOM" => $firstname,
                        "SMS" => $phone_mobile)
            );
        return $this->APISendinblue_send("https://api.sendinblue.com/v3/contacts",$data,"POST");
    }
    
    //Fonction d'envoi de requettes REST API
    //header est un tableau à 2 valeurs
    // 0 : le nom du header
    //1 : la valeur du header
    private function APIREST_send($header,$url,$data=null,$methode="GET")
    {
        if(!isset($url) || $url == null) throw new Exception("API REST URL null");

        //dol_syslog("TiCoop - APIREST_send - methode : $methode",LOG_DEBUG);
        //dol_syslog("TiCoop - APIREST_send - url : $url",LOG_DEBUG);
        //dol_syslog("TiCoop - APIREST_send - data : ". serialize($data),LOG_DEBUG);

        switch(strtoupper($methode))
        {
            case 'PUT' : $fct="put";break;
            case 'POST' : $fct="post";break;
            case 'GET' : $fct="get";
                    if($data != null) $url .= '?'. http_build_query($data);
                    $data=null;
                    break;
            default : throw new Exception("Send methode non connue : " . $methode);
        }
        
        $client = \Httpful\Request::$fct($url)
                    ->addHeader($header[0],$header[1] )
                    ->addHeader('Accept', 'application/json');
        if(!isset($client) || $client == null) throw new Exception("Creation du client REST API en echec");
        if($data != null) 
            $client->addHeader('content-type', 'application/json')
                ->sendsJson()->body(json_encode($data));
        $response = $client->send();
        //dol_syslog("TiCoop - APIREST_send - client header : ". $client->raw_headers,LOG_DEBUG);
        //dol_syslog("TiCoop - APIREST_send - client body : ". $client->raw_body,LOG_DEBUG);
        //dol_syslog("TiCoop - APIREST_send - response header : ". $response->raw_headers,LOG_DEBUG);
        //dol_syslog("TiCoop - APIREST_send - response body : ". $response->raw_body,LOG_DEBUG);
        //si erreur
        if(!isset($response) || $response->meta_data['http_code'] >= 300) throw new Exception("Erreur de requete REST API : ".$response->meta_data['http_code']);
        
        return $response->body;
    }
}

