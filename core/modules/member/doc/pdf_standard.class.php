<?php
/* Copyright (C) 2003 Steve Dillon
 * Copyright (C) 2003 Laurent Passebecq
 * Copyright (C) 2001-2003 Rodolphe Quiedeville <rodolphe@quiedeville.org>
 * Copyright (C) 2002-2003 Jean-Louis Bergamo	<jlb@j1b.org>
 * Copyright (C) 2006-2013 Laurent Destailleur	<eldy@users.sourceforge.net>
 * Copyright (C) 2015 Francis Appels  <francis.appels@yahoo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	\file		htdocs/core/modules/member/doc/pdf_standard.class.php
 *	\ingroup	member
 *	\brief		File of class to generate PDF document of labels
 */

require_once DOL_DOCUMENT_ROOT.'/core/class/commonstickergenerator.class.php';

/**
 *	Class to generate stick sheet with format Avery or other personalised
 */
class pdf_standard extends CommonStickerGenerator
{

	/**
	 * Output a sticker on page at position _COUNTX, _COUNTY (_COUNTX and _COUNTY start from 0)
	 *
	 * @param	PDF			$pdf			PDF reference
	 * @param	Translate	$outputlangs	Output langs
	 * @param	array		$param			Associative array containing label content and optional parameters
	 * @return	void
	 */
    public function addSticker(&$pdf, $outputlangs, $param)
    {
		// use this method in future refactoring
	}

    // phpcs:disable PEAR.NamingConventions.ValidFunctionName.ScopeNotCamelCaps
	/**
	 * Output a sticker on page at position _COUNTX, _COUNTY (_COUNTX and _COUNTY start from 0)
	 * - __LOGO__ is replace with company logo
	 * - __PHOTO__ is replace with photo provided as parameter
	 *
	 * @param	 PDF		$pdf			PDF
	 * @param	 string		$textleft		Text left
	 * @param	 string		$header			Header
	 * @param	 string		$footer			Footer
	 * @param	 Translate	$outputlangs	Output langs
	 * @param	 string		$textright		Text right
	 * @param	 int		$idmember		Id member
	 * @param	 string		$photo			Photo (full path to image file used as replacement for key __PHOTOS__ into left, right, header or footer text)
	 * @return	 void
	 */
	public function Add_PDF_card(&$pdf, $textleft, $header, $footer, $outputlangs, $textright = '', $idmember = 0, $photo = '')
	{
        // phpcs:enable
		global $db,$mysoc,$conf,$langs;
		global $forceimgscalewidth,$forceimgscaleheight;

		$imgscalewidth=(empty($forceimgscalewidth)?0.3:$forceimgscalewidth);	// Scale of image for width (1=Full width of sticker)
		$imgscaleheight=(empty($forceimgscalewidth)?0.5:$forceimgscalewidth);	// Scale of image for height (1=Full height of sticker)

		// We are in a new page, then we must add a page
		if (($this->_COUNTX ==0) && ($this->_COUNTY==0) and (!$this->_First==1)) {
			$pdf->AddPage();
		}
		$this->_First=0;
		$_PosX = $this->_Margin_Left+($this->_COUNTX*($this->_Width+$this->_X_Space));
		$_PosY = $this->_Margin_Top+($this->_COUNTY*($this->_Height+$this->_Y_Space));

		// Define logo
		$logo=$conf->mycompany->dir_output.'/logos/'.$mysoc->logo;
		if (! is_readable($logo))
		{
			$logo='';
			if (! empty($mysoc->logo_small) && is_readable($conf->mycompany->dir_output.'/logos/thumbs/'.$mysoc->logo_small))
			{
				$logo=$conf->mycompany->dir_output.'/logos/thumbs/'.$mysoc->logo_small;
			}
			elseif (! empty($mysoc->logo) && is_readable($conf->mycompany->dir_output.'/logos/'.$mysoc->logo))
			{
				$logo=$conf->mycompany->dir_output.'/logos/'.$mysoc->logo;
			}
		}

		$member=new Adherent($db);
		$member->id = $idmember;
		$member->ref = $idmember;

		// Define photo
		$dir=$conf->adherent->dir_output;
		if (! empty($photo))
		{
			$file=get_exdir(0, 0, 0, 0, $member, 'member').'photos/'.$photo;
			$photo=$dir.'/'.$file;
			if (! is_readable($photo)) $photo='';
		}

		// Define background image
		$backgroundimage='';
		if(! empty($conf->global->ADHERENT_CARD_BACKGROUND) && file_exists($conf->adherent->dir_output.'/'.$conf->global->ADHERENT_CARD_BACKGROUND))
		{
			$backgroundimage=$conf->adherent->dir_output.'/'.$conf->global->ADHERENT_CARD_BACKGROUND;
		}

		// Print lines
		if ($this->code == "CARD")
		{
			$this->Tformat=$this->_Avery_Labels["CARD"];
			//$this->_Pointille($pdf,$_PosX,$_PosY,$_PosX+$this->_Width,$_PosY+$this->_Height,0.3,25);
			$this->_Croix($pdf, $_PosX, $_PosY, $_PosX+$this->_Width, $_PosY+$this->_Height, 0.1, 10);
		}

		// Background
		if ($backgroundimage)
		{
			$pdf->image($backgroundimage, $_PosX, $_PosY, $this->_Width, $this->_Height);
		}

		$xleft=2; $ytop=2;

		// Top
		if ($header!='')
		{
			if ($this->code == "CARD")
			{
				$pdf->SetDrawColor(128, 128, 128);
				$pdf->Line($_PosX, $_PosY+$this->_Line_Height+1, $_PosX+$this->_Width, $_PosY+$this->_Line_Height+1); // Only 1 mm and not ytop for top text
				$pdf->SetDrawColor(0, 0, 0);
			}
			$pdf->SetXY($_PosX+$xleft, $_PosY+1); // Only 1 mm and not ytop for top text
			$pdf->Cell($this->_Width-2*$xleft, $this->_Line_Height, $outputlangs->convToOutputCharset($header), 0, 1, 'C');
		}


		$ytop+=(empty($header)?0:(1+$this->_Line_Height));

		// Define widthtouse and heighttouse
		$maxwidthtouse=round(($this->_Width - 2*$xleft)*$imgscalewidth); $maxheighttouse=round(($this->_Height - 2*$ytop)*$imgscaleheight);
		$defaultratio=($maxwidthtouse/$maxheighttouse);
		$widthtouse=$maxwidthtouse; $heighttouse=0;		// old value for image
		$tmp=dol_getImageSize($photo, false);
		if ($tmp['height'])
		{
			$imgratio=$tmp['width']/$tmp['height'];
			if ($imgratio >= $defaultratio) { $widthtouse = $maxwidthtouse; $heighttouse = round($widthtouse / $imgratio); }
			else { $heightouse = $maxheighttouse; $widthtouse = round($heightouse * $imgratio); }
		}
		//var_dump($this->_Width.'x'.$this->_Height.' with border and scale '.$imgscale.' => max '.$maxwidthtouse.'x'.$maxheighttouse.' => We use '.$widthtouse.'x'.$heighttouse);exit;

		// Center
		if ($textright=='')	// Only a left part
		{
			// Output left area
			if ($textleft == '__LOGO__' && $logo) $pdf->Image($logo, $_PosX+$xleft, $_PosY+$ytop, $widthtouse, $heighttouse);
			elseif ($textleft == '__PHOTO__' && $photo) $pdf->Image($photo, $_PosX+$xleft, $_PosY+$ytop, $widthtouse, $heighttouse);
			else
			{
				$pdf->SetXY($_PosX+$xleft, $_PosY+$ytop);
				$pdf->MultiCell($this->_Width, $this->_Line_Height, $outputlangs->convToOutputCharset($textleft), 0, 'L');
			}
		}
		elseif ($textleft!='' && $textright!='')	//
		{
			if ($textleft == '__LOGO__' || $textleft == '__PHOTO__')
			{
				if ($textleft == '__LOGO__' && $logo) $pdf->Image($logo, $_PosX+$xleft, $_PosY+$ytop, $widthtouse, $heighttouse);
				elseif ($textleft == '__PHOTO__' && $photo) $pdf->Image($photo, $_PosX+$xleft, $_PosY+$ytop, $widthtouse, $heighttouse);
				$pdf->SetXY($_PosX+$xleft+$widthtouse+1, $_PosY+$ytop);
				$pdf->MultiCell($this->_Width-$xleft-$xleft-$widthtouse-1, $this->_Line_Height, $outputlangs->convToOutputCharset($textright), 0, 'R');
			}
			elseif ($textright == '__LOGO__' || $textright == '__PHOTO__')
			{
				if ($textright == '__LOGO__' && $logo) $pdf->Image($logo, $_PosX+$this->_Width-$widthtouse-$xleft, $_PosY+$ytop, $widthtouse, $heighttouse);
				elseif ($textright == '__PHOTO__' && $photo) $pdf->Image($photo, $_PosX+$this->_Width-$widthtouse-$xleft, $_PosY+$ytop, $widthtouse, $heighttouse);
				$pdf->SetXY($_PosX+$xleft, $_PosY+$ytop);
				$pdf->MultiCell($this->_Width-$widthtouse-$xleft-$xleft-1, $this->_Line_Height, $outputlangs->convToOutputCharset($textleft), 0, 'L');
			}
			else	// text on halft left and text on half right
			{
				$pdf->SetXY($_PosX+$xleft, $_PosY+$ytop);
				$pdf->MultiCell(round($this->_Width/2), $this->_Line_Height, $outputlangs->convToOutputCharset($textleft), 0, 'L');
				$pdf->SetXY($_PosX+round($this->_Width/2), $_PosY+$ytop);
				$pdf->MultiCell(round($this->_Width/2)-2, $this->_Line_Height, $outputlangs->convToOutputCharset($textright), 0, 'R');
			}
		}
		else	// Only a right part
		{
			// Output right area
			if ($textright == '__LOGO__' && $logo) $pdf->Image($logo, $_PosX+$this->_Width-$widthtouse-$xleft, $_PosY+$ytop, $widthtouse, $heighttouse);
			elseif ($textright == '__PHOTO__' && $photo) $pdf->Image($photo, $_PosX+$this->_Width-$widthtouse-$xleft, $_PosY+$ytop, $widthtouse, $heighttouse);
			else
			{
				$pdf->SetXY($_PosX+$xleft, $_PosY+$ytop);
				$pdf->MultiCell($this->_Width-$xleft, $this->_Line_Height, $outputlangs->convToOutputCharset($textright), 0, 'R');
			}
		}

		// Bottom
		if ($footer!='')
		{
			if ($this->code == "CARD")
			{
				$pdf->SetDrawColor(128, 128, 128);
				$pdf->Line($_PosX, $_PosY+$this->_Height-$this->_Line_Height-2, $_PosX+$this->_Width, $_PosY+$this->_Height-$this->_Line_Height-2);
				$pdf->SetDrawColor(0, 0, 0);
			}
			$pdf->SetXY($_PosX, $_PosY+$this->_Height-$this->_Line_Height-1);
			$pdf->Cell($this->_Width, $this->_Line_Height, $outputlangs->convToOutputCharset($footer), 0, 1, 'C');
		}
		//print "$_PosY+$this->_Height-$this->_Line_Height-1<br>\n";

		$this->_COUNTY++;

		if ($this->_COUNTY == $this->_Y_Number) {
			// Si on est en bas de page, on remonte le 'curseur' de position
			$this->_COUNTX++;
			$this->_COUNTY=0;
		}

		if ($this->_COUNTX == $this->_X_Number) {
			// Si on est en bout de page, alors on repart sur une nouvelle page
			$this->_COUNTX=0;
			$this->_COUNTY=0;
		}
	}

    // phpcs:disable PEAR.NamingConventions.ValidFunctionName.ScopeNotCamelCaps
	/**
	 *	Function to build PDF on disk, then output on HTTP stream.
	 *
	 *	@param	Adherent	$object		        Member object. Old usage: Array of record informations (array('textleft'=>,'textheader'=>, ...'id'=>,'photo'=>)
	 *	@param	Translate	$outputlangs		Lang object for output language
	 *	@param	string		$srctemplatepath	Full path of source filename for generator using a template file. Example: '5161', 'AVERYC32010', 'CARD', ...
	 *	@param	string		$mode				Tell if doc module is called for 'member', ...
	 *  @param  int         $nooutput           1=Generate only file on disk and do not return it on response
	 *	@return	int								1=OK, 0=KO
	 */
	public function write_file($object, $outputlangs, $srctemplatepath, $mode = 'member', $nooutput = 0)
	{
		//Ajout TiCoop 
		//vi pdf_standard.class.php
		//docker exec dolibarr10form_web_1 ls /var/www/html/core/modules/member/doc/
		//docker cp ./pdf_standard.class.php dolibarr10form_web_1:/var/www/html/core/modules/member/doc/pdf_standard.class.php
		
		//Création de la carte membre
		dol_syslog("TiCoop - Creation des cartes TiCoop, fonction qui remplace celle de dolibarr");
		$interror = 1;
		dol_syslog("TiCoop - Creation la carte membre");
		//dol_syslog("TiCoop - Force le style CARD (CB)");
		//$srctemplatepath='CARD';
		$interror =$this->TiCoop_PDF($object,$srctemplatepath);
		//Création des cartes foyer, soit tout les fichiers qui commencent par foyer.*
		dol_syslog("TiCoop - Creation des cartes foyer, si exsitantes");
		$listFiles = $this->getFoyerPhoto($object);
		foreach($listFiles as $photo)
		{
			dol_syslog("TiCoop - Creation de la carte foyer : ".$photo['name']);
			#Le nom du fichier doit être sous la forme : foyer.Prenom.NOM.xxx
			$tab = explode('.',$photo['name']);
			//mise en forme
			$prenom = ucfirst(strtolower($tab['1']));
			$nom = strtoupper($tab['2']);
		
			$altInfo = Array('firstname'=>$prenom, 'lastname' => $nom, 'photo' => $photo['fullname']);
			$interror =$this->TiCoop_PDF($object,$srctemplatepath, $altInfo);
		}
		dol_syslog("TiCoop - Fin");
		return $interror;
		///Ti COOP FIn de la mdification (hors fonctions ci-dessous)
		

        // phpcs:enable
		global $user,$conf,$langs,$mysoc,$_Avery_Labels;

		$this->code=$srctemplatepath;

		if (is_object($object))
		{
		    if ($object->country == '-') $object->country='';

    		// List of values to scan for a replacement
    		$substitutionarray = array (
    		    '__ID__'=>$object->rowid,
    		    '__LOGIN__'=>$object->login,
    		    '__FIRSTNAME__'=>$object->firstname,
    		    '__LASTNAME__'=>$object->lastname,
    		    '__FULLNAME__'=>$object->getFullName($langs),
    		    '__COMPANY__'=>$object->company,
    		    '__ADDRESS__'=>$object->address,
    		    '__ZIP__'=>$object->zip,
    		    '__TOWN__'=>$object->town,
    		    '__COUNTRY__'=>$object->country,
    		    '__COUNTRY_CODE__'=>$object->country_code,
    		    '__EMAIL__'=>$object->email,
    		    '__BIRTH__'=>dol_print_date($object->birth, 'day'),
    		    '__TYPE__'=>$object->type,
    		    '__YEAR__'=>$year,
    		    '__MONTH__'=>$month,
    		    '__DAY__'=>$day,
    		    '__DOL_MAIN_URL_ROOT__'=>DOL_MAIN_URL_ROOT,
    		    '__SERVER__'=>"http://".$_SERVER["SERVER_NAME"]."/"
    		);
    		complete_substitutions_array($substitutionarray, $langs);

    		// For business cards
		    $textleft=make_substitutions($conf->global->ADHERENT_CARD_TEXT, $substitutionarray);
		    $textheader=make_substitutions($conf->global->ADHERENT_CARD_HEADER_TEXT, $substitutionarray);
		    $textfooter=make_substitutions($conf->global->ADHERENT_CARD_FOOTER_TEXT, $substitutionarray);
		    $textright=make_substitutions($conf->global->ADHERENT_CARD_TEXT_RIGHT, $substitutionarray);

		    $nb = $_Avery_Labels[$this->code]['NX'] * $_Avery_Labels[$this->code]['NY'];
		    if ($nb <= 0) $nb=1;  // Protection to avoid empty page

		    for($j=0;$j<$nb;$j++)
	        {
	            $arrayofmembers[]=array(
	                'textleft'=>$textleft,
	                'textheader'=>$textheader,
	                'textfooter'=>$textfooter,
	                'textright'=>$textright,
	                'id'=>$object->rowid,
	                'photo'=>$object->photo
	            );
	        }

    		$arrayofrecords = $arrayofmembers;
		}
		else
		{
		    $arrayofrecords = $object;
		}

		//var_dump($arrayofrecords);exit;

		$this->Tformat = $_Avery_Labels[$this->code];
		if (empty($this->Tformat)) { dol_print_error('', 'ErrorBadTypeForCard'.$this->code); exit; }
		$this->type = 'pdf';
        // standard format or custom
        if ($this->Tformat['paper-size']!='custom') {
            $this->format = $this->Tformat['paper-size'];
        } else {
            //custom
            $resolution= array($this->Tformat['custom_x'], $this->Tformat['custom_y']);
            $this->format = $resolution;
        }

		if (! is_object($outputlangs)) $outputlangs=$langs;
		// For backward compatibility with FPDF, force output charset to ISO, because FPDF expect text to be encoded in ISO
		if (! empty($conf->global->MAIN_USE_FPDF)) $outputlangs->charset_output='ISO-8859-1';

		// Load traductions files requiredby by page
		$outputlangs->loadLangs(array("main", "dict", "companies", "admin", "members"));

		if (empty($mode) || $mode == 'member')
		{
			$title=$outputlangs->transnoentities('MembersCards');
			$keywords=$outputlangs->transnoentities('MembersCards')." ".$outputlangs->transnoentities("Foundation")." ".$outputlangs->convToOutputCharset($mysoc->name);
		}
		else
		{
			dol_print_error('', 'Bad value for $mode');
			return -1;
		}

		$filename = 'tmp_cards.pdf';
		if (is_object($object))
		{
		    $outputdir = $conf->adherent->dir_output;
		    $dir = $outputdir."/".get_exdir(0, 0, 0, 0, $object, 'member');
		    $file = $dir.'/'.$filename;
		}
		else
		{
		    $outputdir = $conf->adherent->dir_temp;
		    $dir = $outputdir;
		    $file = $dir.'/'.$filename;
		}

		//var_dump($file);exit;

		if (! file_exists($dir))
		{
			if (dol_mkdir($dir) < 0)
			{
				$this->error=$langs->trans("ErrorCanNotCreateDir", $dir);
				return 0;
			}
		}

		$pdf=pdf_getInstance($this->format, $this->Tformat['metric'], $this->Tformat['orientation']);

		if (class_exists('TCPDF'))
		{
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
		}
		$pdf->SetFont(pdf_getPDFFont($outputlangs));

		$pdf->SetTitle($title);
		$pdf->SetSubject($title);
		$pdf->SetCreator("Dolibarr ".DOL_VERSION);
		$pdf->SetAuthor($outputlangs->convToOutputCharset($user->getFullName($outputlangs)));
		$pdf->SetKeyWords($keywords);
		if (! empty($conf->global->MAIN_DISABLE_PDF_COMPRESSION)) $pdf->SetCompression(false);

		$pdf->SetMargins(0, 0);
		$pdf->SetAutoPageBreak(false);

		$this->_Metric_Doc = $this->Tformat['metric'];
		// Permet de commencer l'impression de l'etiquette desiree dans le cas ou la page a deja servie
		$posX=1;
		$posY=1;
		if ($posX > 0) $posX--; else $posX=0;
		if ($posY > 0) $posY--; else $posY=0;
		$this->_COUNTX = $posX;
		$this->_COUNTY = $posY;
		$this->_Set_Format($pdf, $this->Tformat);


		$pdf->Open();
		$pdf->AddPage();


		// Add each record
		foreach($arrayofrecords as $val)
		{
			// imprime le texte specifique sur la carte
			$this->Add_PDF_card($pdf, $val['textleft'], $val['textheader'], $val['textfooter'], $langs, $val['textright'], $val['id'], $val['photo']);
		}

		//$pdf->SetXY(10, 295);
		//$pdf->Cell($this->_Width, $this->_Line_Height, 'XXX',0,1,'C');


		// Output to file
		$pdf->Output($file, 'F');

		if (! empty($conf->global->MAIN_UMASK))
			@chmod($file, octdec($conf->global->MAIN_UMASK));


		$this->result = array('fullpath'=>$file);

		// Output to http stream
		if (empty($nooutput))
		{
    		clearstatcache();

    		$attachment=true;
    		if (! empty($conf->global->MAIN_DISABLE_FORCE_SAVEAS)) $attachment=false;
    		$type=dol_mimetype($filename);

    		//if ($encoding)   header('Content-Encoding: '.$encoding);
    		if ($type)		 header('Content-Type: '.$type);
    		if ($attachment) header('Content-Disposition: attachment; filename="'.$filename.'"');
    		else header('Content-Disposition: inline; filename="'.$filename.'"');

    		// Ajout directives pour resoudre bug IE
    		header('Cache-Control: Public, must-revalidate');
    		header('Pragma: public');

    		readfile($file);
		}

		return 1;
	}

	/*	Function to build PDF on disk, then output on HTTP stream.
		*
		*	@param	Adherent	$object		        Member object. Old usage: Array of record informations (array('textleft'=>,'textheader'=>, ...'id'=>,'photo'=>)
		*	@param	string		$code/code	Full path of source filename for generator using a template file. Example: '5161', 'AVERYC32010', 'CARD', ...
		*	@return	int								1=OK, 0=KO
		*/
	private function TiCoop_PDF ($object,$code,$altInfo=null)
	{
		//exit if error
		if (!is_object($object))
		{
			//pour le moment la fonction ne sait gérer que un utilisateur à la fois.
			$this->error="La fonction ne sait gérer que un utilisateur à la fois";
			return 0;
		}

		if (!isset($code) || $code == null)
		{
			$this->error="Identification du model de PDF vide";
			return 0;
		}

		// phpcs:enable
		global $user,$conf,$langs,$mysoc,$_Avery_Labels;

		// Load traductions files requiredby by page
		$langs->loadLangs(array("main", "dict", "companies", "admin", "members"));
		// For backward compatibility with FPDF, force output charset to ISO, because FPDF expect text to be encoded in ISO
		if (! empty($conf->global->MAIN_USE_FPDF)) $langs->charset_output='ISO-8859-1';
				
		//Défintion du chemin de mémorisation du fichier PDF (dans le dossier PJ du membre)
		$filename = 'carteMembre.pdf';
		if(isset ($altInfo['firstname'])) $filename = 'carteFoyer_'.$altInfo['firstname'].'_'.$altInfo['lastname'].'.pdf';
		
	    $dir = $conf->adherent->dir_output."/".get_exdir(0, 0, 0, 0, $object, 'member');
	    $file = $dir.'/'.$filename;
		if (! file_exists($dir))
		{
			if (dol_mkdir($dir) < 0)
			{
				$this->error=$langs->trans("ErrorCanNotCreateDir", $dir);
				return 0;
			}
		}

		//Création du PDF
		//https://github.com/Dolibarr/dolibarr/blob/a7390e7e474f2a791ff570eab739a8bdb6656ecc/htdocs/core/lib/pdf.lib.php
		$pdf=pdf_getInstance("A4");
		if (class_exists('TCPDF'))
		{
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
		}
		$pdf->SetFont(pdf_getPDFFont($langs));
		$pdf->SetTitle($filename);
		$pdf->SetSubject($filename);
		$pdf->SetCreator("Ti Coop");
		$pdf->SetAuthor($user->getFullName($langs));
		$pdf->SetKeyWords($filename);
		if (!empty($conf->global->MAIN_DISABLE_PDF_COMPRESSION)) $pdf->SetCompression(false);
		$pdf->SetMargins(0, 0);
		$pdf->SetAutoPageBreak(false);
		//récupération des dimensions des étiquettes cibles 
		//https://github.com/Dolibarr/dolibarr/blob/develop/htdocs/core/class/commonstickergenerator.class.php
		$this->_Set_Format($pdf, $_Avery_Labels[$code]);
		/*
		$this->_Metric = $_Avery_Labels['metric'];
		$this->_Avery_Name = $_Avery_Labels['name'];
		$this->_Avery_Code = $_Avery_Labels['code'];
		$this->_Margin_Left	= $this->convertMetric($_Avery_Labels['marginLeft'], $this->_Metric, $this->_Metric_Doc);
		$this->_Margin_Top = $this->convertMetric($_Avery_Labels['marginTop'], $this->_Metric, $this->_Metric_Doc);
		$this->_X_Space = $this->convertMetric($_Avery_Labels['SpaceX'], $this->_Metric, $this->_Metric_Doc);
		$this->_Y_Space = $this->convertMetric($_Avery_Labels['SpaceY'], $this->_Metric, $this->_Metric_Doc);
		$this->_X_Number = $_Avery_Labels['NX'];
		$this->_Y_Number = $_Avery_Labels['NY'];
		$this->_Width = $this->convertMetric($_Avery_Labels['width'], $this->_Metric, $this->_Metric_Doc);
		$this->_Height = $this->convertMetric($_Avery_Labels['height'], $this->_Metric, $this->_Metric_Doc);
		$this->Set_Char_Size($pdf, $_Avery_Labels['font-size']);
		*/
		//Prépartion à l'écriture
		$pdf->Open();
		$pdf->AddPage();

		//Ecriture dans le PDF
		try
		{
			//Création d'une carte membre
			for($i=0;$i<5;$i++)
			{
				//création de 5 pages dans le PDF afin de pouvoir choisire sur quelle étiquette imprimer
				$this->TiCoop_PDF_MemberCard($pdf,$object,$this->_Margin_Left,$this->_Margin_Top + $i * ($this->_Height + $this->_X_Number),$altInfo);
				if($i<4)$pdf->AddPage();
			}
		}
		catch (Exception $e) 
		{
			$this->error=$e->getMessage();
			return 0;
		}
		//Output to fileTiCoop
		$pdf->Output($file, 'F');
		if (! empty($conf->global->MAIN_UMASK)) @chmod($file, octdec($conf->global->MAIN_UMASK));

		$this->result = array('fullpath'=>$file);

	   return 1;
	}

	//object = Member object
	//position et hauteur en mm
	//altInfo : info de substition à celle de l'objet.
	private function TiCoop_PDF_MemberCard($pdf,$object,$posX=0,$posY=0, $altInfo=null)
	{
		
		//carte crédit : 85,60 × 53,98 mm (3,375"x2,125)=> https://fr.wikipedia.org/wiki/ISO/CEI_7810
		//papier :  85 mm x 54 mm => ex : Avery C32011-10PR Etiquettes 85 x 54 mm Blanc (A4 2x5)
		
		/*
		Différene entre les deux type de papier
		https://github.com/Dolibarr/dolibarr/blob/develop/htdocs/install/mysql/data/llx_c_format_cards.sql
		https://github.com/Dolibarr/dolibarr/blob/develop/htdocs/core/lib/format_cards.lib.php
		name	 					topmargin	 spacex
		'Avery-C32010'	 			13.00000000	 10.00000000
		  'Dolibarr Business cards'	15.00000000	 0.00000000

		  ==> nouveux papaier en 80x50
		*/
		global $db,$user,$conf,$langs,$mysoc;
		/* Pour info
			'__ID__'=>$object->rowid,
			'__LOGIN__'=>$object->login,
			'__FIRSTNAME__'=>$object->firstname,
			'__LASTNAME__'=>$object->lastname,
			'__FULLNAME__'=>$object->getFullName($langs),
			'__COMPANY__'=>$object->company,
			'__ADDRESS__'=>$object->address,
			'__ZIP__'=>$object->zip,
			'__TOWN__'=>$object->town,
			'__COUNTRY__'=>$object->country,
			'__COUNTRY_CODE__'=>$object->country_code,
			'__EMAIL__'=>$object->email,
			'__BIRTH__'=>dol_print_date($object->birth, 'day'),
			'__TYPE__'=>$object->type,
			'__YEAR__'=>$year,
			'__MONTH__'=>$month,posXposXposX
			'__DAY__'=>$day,
			'__DOL_MAIN_URL_ROOT__'=>DOL_MAIN_URL_ROOT,
			'__SERVER__'=>"http://".$_SERVER["SERVER_NAME"]."/"
			'photo'=>$object->photo //nom de la photo
			'logo' => $this->getLogoPath();
			$photoPath = $this->getUserPhoto($object);
			
		*/
		
		//mise en page
		//https://github.com/Dolibarr/dolibarr/blob/develop/htdocs/core/class/commonstickergenerator.class.php
		$padding = 1;//2 trop ?
		$posX 	+= $padding;
		$posY 	+= 0;
		$currentX = $posX;
		$currentY = $posY;
		$thirdCartdWidth = $this->_Width / 3;

		/*$pdf->SetXY(0 , 0);
		$pdf->Cell(0, 0, "Carte l".$this->_Width." h".$this->_Height, 0, 1, 'L');
		$pdf->Line($posX, $posY, $posX+$this->_Width, $posY); //ligne haut
		$pdf->Line($posX, $posY+$this->_Height, $posX+$this->_Width, $posY+$this->_Height); //ligne bas
		$pdf->Line($posX+$this->_Width, $posY, $posX+$this->_Width, $posY+$this->_Height); //ligne droite
		$pdf->Line($posX, $posY, $posX, $posY+$this->_Height); //ligne gauche
		$pdf->Line($posX+($this->_Width/2), $posY, $posX+($this->_Width/2), $posY+$this->_Height); //ligne milieu
		*/
		//Gestion des entrants
		if($object == null) throw new Exception('Objet membre null');
		//Récupération des composant necessaires 
		//Photo
		$photoPath = $this->getUserPhoto($object);
		if($photoPath == null) throw new Exception('Photo manquante pour le membre');
		//Prénom
		if($object->firstname == null) throw new Exception('Prenom manquant pour le membre');
		$prenom = ucfirst(strtolower($object->firstname));
		//Nom
		if($object->lastname == null) throw new Exception('Nom manquant pour le membre');
		$nom = strtoupper($object->lastname);
		//Login
		if($object->login == null) throw new Exception('Login manquant pour le membre');
		$login = strtolower($object->login);

		//Défintion des valeurs alt
		$cardTitle = "Carte Cooperateur";
		$isFoyer = 0;
		if(isset ($altInfo))
		{
			$isFoyer = 1;
			$cardTitle = "Carte Rataché";//ancien Foyer
		} 
		if(isset ($altInfo['firstname'])) $prenom = ucfirst(strtolower($altInfo['firstname']));
		if(isset ($altInfo['lastname'])) $nom = strtoupper($altInfo['lastname']);
		if(isset ($altInfo['photo'])) $photoPath = $altInfo['photo'];

		///l/ pour link, création d'url courte avec redirection vers le service voulu. Permet de faire abstraction de l'URL cible.
		$qrcodeURL = "http://l.ticoop.fr/carte?login=".urlencode($login);
			$qrcodeURL .= '&nom='.urlencode($nom);
			$qrcodeURL .= '&prenom='.urlencode($prenom);
			$qrcodeURL .= '&isF='.urlencode($isFoyer);//isFoyer ou Rataché
			$qrcodeURL .= '&time='.urlencode(time());

		//La carte est composée de trois parties, horizontallement
		/*
		 ___________________________________________
		|              Titre de la carte			|
		|  				| CodeBare ID |  QRCode		|
		| Photo profil	| Prénom Nom				|
		|___________________________________________|
		*/

		//$pdf->SetXY($posX,$posY);$pdf->Cell($padding, 0, "+", 0, 1, 'L', 0, '', 0);
		//$pdf->SetXY($posX+$this->_Width, $posY);$pdf->Cell($padding, 0, "+", 0, 1, 'L', 0, '', 0);
		//$pdf->SetXY($posX, $posY+$this->_Height);$pdf->Cell($padding, 0, "+", 0, 1, 'L', 0, '', 0);
		//$pdf->SetXY($posX+$this->_Width, $posY+$this->_Height);$pdf->Cell($padding, 0, "+", 0, 1, 'L', 0, '', 0);

		//--------------- Titre
		//defintion du point de repere
		$pdf->SetXY($currentX , $currentY);
		//$saveCharSize = $this->_Char_Size;
		$this->Set_Char_Size($pdf, 15);
		//creation d'un rectangle à partir de ce point de repere
		$pdf->Cell($this->_Width  - 2 * $padding, $this->_Line_Height, $cardTitle, 0, 1, 'C');
		$titleHeight = $this->_Line_Height + $padding;
		$topY = $currentY += $titleHeight;
		$this->Set_Char_Size($pdf, 11);

		//--------------- Ligne Haut
		//-----Affiche la photo de l'utilisateur
		#dol_syslog("TiCoop - Dimmentionnement photo : $photoPath");
		$maxImageHeight = $this->_Height - ($titleHeight + $padding);//- $this->_Line_Height//1 ligneq après ( prenom, nom) 
		$photoSize=$this->getPhotoUsableSize($photoPath,$thirdCartdWidth,$maxImageHeight);
		$resolution = 6;//valeur empirique
		$tmpPhotoPath = $this->resize_image($photoPath, $photoSize['width']*$resolution, $photoSize['height']*$resolution); 
		$pdf->Image($tmpPhotoPath, $currentX + $padding, $currentY, $photoSize['width'], $photoSize['height']);
		$currentX += $photoSize['width'] + $padding;

		
		//------ Inégreation du codebarre utilisateure
		//le code utilisateur s'adapte en fonction de la photo
		//var_dump($object);
		$codeBarrewidth= $thirdCartdWidth * 2 - $photoSize['width'] - 5 * $padding - 4; //2/3 de la carte, moins la taille de la photo - de l'esapce pour le scan
		$idbarrecodePath = $this->getUserIdCode($object->id,$codeBarrewidth,$codeBarrewidth);
		if($idbarrecodePath == null) throw new Exception('Code barre ID manquante pour le membre');
		//$idbarrecodePadding = (($this->_Width / 2) - $idbarrecodeSize ) /2 ;
		$pdf->Image($idbarrecodePath, $currentX + $padding + 2, $currentY, $codeBarrewidth,$codeBarrewidth);
		$posTextX = $currentX;
		$currentX = $currentX + $codeBarrewidth + $padding + 4;
		//supprime le fichier barcode
		unlink($idbarrecodePath);

		//----- Génération du QRCode
		$qrcodwidth = $thirdCartdWidth-$padding;//1 seul padding l'autre est fait  par le qrcode
		$qrcodePath = $this->getUserQRCode($qrcodeURL,$qrcodwidth,$qrcodwidth);
		if($qrcodePath == null) throw new Exception('QRCode manquante pour le membre');
		$pdf->Image($qrcodePath,$currentX + $padding, $currentY, $qrcodwidth,$qrcodwidth);//postion x depuis la fin de la carte
		$currentX = $currentX + $qrcodwidth;
		$posTextY = $currentY + $qrcodwidth;
		//supprime le fichier barcode
		unlink($qrcodePath);

		//--------------- Bas
		//Affiche le prénom 
		$currentX = $posTextX + $padding;
		$currentY = $posTextY + $padding;
		$pdf->SetXY($currentX, $currentY);
		//Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=0, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M')
		$pdf->Cell($padding, 0, $prenom, 0, 1, 'L', 0, '', 0);
		$currentY += $this->_Line_Height ;
		//et le nom
		$pdf->SetXY($currentX, $currentY);
		$pdf->Cell($padding, 0, $nom, 0, 1, 'L', 0, '', 0);	
		//$currentY += $this->_Line_Height ;	
	}

	//Fonction qui récupére le chemin vers le logo ERP.
	private function getLogoPath()
	{
		global $user,$conf,$langs,$mysoc;
		$logo=$conf->mycompany->dir_output.'/logos/'.$mysoc->logo;
		if (! is_readable($logo))
		{
			$logo='';
			if (! empty($mysoc->logo_small) && is_readable($conf->mycompany->dir_output.'/logos/thumbs/'.$mysoc->logo_small))
			{
				$logo=$conf->mycompany->dir_output.'/logos/thumbs/'.$mysoc->logo_small;
			}
			elseif (! empty($mysoc->logo) && is_readable($conf->mycompany->dir_output.'/logos/'.$mysoc->logo))
			{
				$logo=$conf->mycompany->dir_output.'/logos/'.$mysoc->logo;
			}
		}
		return $logo;
	}

	//Fonction de récupére le chemin vers la photo de l'utilisateur
	private function getUserPhoto($object)
	{
		global $conf;
		// Define photo path
		if (! empty($object->photo))
		{
			$file=get_exdir(0, 0, 0, 0, $object, 'member').'photos/'.$object->photo;
			$photo=$conf->adherent->dir_output.'/'.$file;
			if (! is_readable($photo)) return null;
			return $photo;
		}
		return null;
	}

	//Fonction de récupére la liste des photos des foyers utilisateurs pour créer les cartes foyers
	private function getFoyerPhoto($object,$filter='^(foyer.*)$')
	{
		require_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';
		global $conf;
		$upload_dir = $conf->adherent->dir_output . "/" . get_exdir(0, 0, 0, 1, $object, 'member');
		// 0 = non recusif
		//filter
		//exclude filter (default one)
		//return Array of array('name'=>'xxx','fullname'=>'/abc/xxx','date'=>'yyy','size'=>99,'type'=>'dir|file',...)
		$filearray=dol_dir_list($upload_dir, "files", 0, $filter, '(\.meta|_preview.*\.png)$');
		return $filearray;
	}

	//Fonction de récupération de la taille appropriée des photos
	private function getPhotoUsableSize($photoPath,$maxPhotoWidth,$maxPhotoHeight)
	{
		$defaultratio=($maxPhotoWidth/$maxPhotoHeight);
		$photoSize=dol_getImageSize($photoPath, false);
		if ($photoSize['height'])
		{
			$imgratio=$photoSize['width']/$photoSize['height'];
			if ($imgratio >= $defaultratio) 
			{
				$photoWidth = $maxPhotoWidth; 
				$photoHeight = round($photoWidth / $imgratio); 
			}
			else
			{ 
				$photoHeight = $maxPhotoHeight; 
				$photoWidth = round($photoHeight * $imgratio); 
			}
		}
		return array('width' => $photoWidth, 'height' => $photoHeight);
	}

	private function getUserQRCode($text,$width=1, $height=1,$color= array(0,0,0))
	{
		return $this->getBarecode($text,$width, $height,$color,'QRCODE,L');
	}
	private function getUserIdCode($text,$width=1, $height=1,$color= array(0,0,0))
	{
		#EAN13 est sur 11 chiffre + la clef, - 3 du préfix client d'Oddo = 8 chiffre
		$text = sprintf("%09d",$text);
		return $this->getBarecode("042".$text,$width, $height,$color,"EAN13");
	}
	private function getBarecode($text,$width=1, $height=1,$color= array(0,0,0), $tcpdfEncoding="EAN13")
	{
		//https://github.com/Dolibarr/dolibarr/blob/37d692918badd3bfb8ed45503e0e833832c2e427/htdocs/adherents/cartes/carte.php
		//https://github.com/Dolibarr/dolibarr/blob/develop/htdocs/core/modules/member/doc/pdf_standard.class.php
		//https://github.com/Dolibarr/dolibarr/blob/develop/htdocs/core/modules/barcode/doc/tcpdfbarcode.modules.php
		//https://github.com/Dolibarr/dolibarr/blob/a7bd734ece65606a657d1bcfb242909c43caad1b/htdocs/admin/barcode.php

		global $conf;
		require_once DOL_DOCUMENT_ROOT.'/core/modules/barcode/doc/tcpdfbarcode.modules.php';

		/*
		//pas d'utilisaion de writeBarCode car le text est utilsé pour créer le path de l'image
		$module = new modTcpdfbarcode($db);
		$result=$module->writeBarCode($text, $encoding, 'Y');
		*/

		//Défintion du dossier de génération des codes
		dol_mkdir($conf->barcode->dir_temp);
		//$file=$conf->barcode->dir_temp.'/barcode_'.$code.'_'.$encoding.'.png';
		$file = tempnam($conf->barcode->dir_temp, "barcode_").'.png';

		//Initialisation de l'objet
		//$tcpdfEncoding='QRCODE,L'; //see $module->getTcpdfEncodingType()
		// TODO : Gérer mieux la distinction 2D, 3D
		if($tcpdfEncoding === "EAN13")
		{
			require_once TCPDF_PATH.'/tcpdf_barcodes_1d.php';
			$barcodeobj = new TCPDFBarcode($text, $tcpdfEncoding);
		}
		else
		{	
			require_once TCPDF_PATH.'/tcpdf_barcodes_2d.php';
			$barcodeobj = new TCPDF2DBarcode($text, $tcpdfEncoding);
		}

		//Création du QRCode
		$imageData = $barcodeobj->getBarcodePngData($width, $height, $color);
		if (!$imageData ) throw new Exception('Erreur lors de la generation du '.$tcpdfEncoding);

		//Conversion en image
		if (function_exists('imagecreate')) 
		{
			$imageData = imagecreatefromstring($imageData);
		}
		if (!imagepng($imageData, $file)) throw new Exception("Erreur lors de l'éciture  du ".$tcpdfEncoding." : ".$file);

		return $file;		
	}

	//réduction de l'image afin de rédure le poid de la carte et faciliter son impression
	function resize_image($file, $w, $h, $crop=FALSE) 
	{
		if(!list($width, $height) = getimagesize($file)) throw new Exception( "Unsupported picture type!");
		$r = $width / $height;
		if ($crop) {
			if ($width > $height) {
				$width = ceil($width-($width*abs($r-$w/$h)));
			} else {
				$height = ceil($height-($height*abs($r-$w/$h)));
			}
			$newwidth = $w;
			$newheight = $h;
		} else {
			if ($w/$h > $r) {
				$newwidth = $h*$r;
				$newheight = $h;
			} else {
				$newheight = $w/$r;
				$newwidth = $w;
			}
		}
		
		#récupération de l'image source
		#dol_syslog("TiCoop - resize_image '".strtolower(pathinfo($file, PATHINFO_EXTENSION))."'");
		switch(strtolower(pathinfo($file, PATHINFO_EXTENSION)))
		{
			case "png":
				$src = imagecreatefrompng($file);
				break;
			case "jpg":
			case "jpeg":
				$src = imagecreatefromjpeg($file);
				break;
			case "bmp":
				$src = imagecreatefrombmp($file);
				break;
			case "gif":
				$src = imagecreatefromgif($file);
				break;
			default:
				//$src = imagecreatefromstring($file);
				throw new Exception("Erreur lors du traitement de le photo de profil - EXTENSION non connue (png | jpg,jpeg | bmp | gif) " );
		}		
		if (!$src ) throw new Exception("Erreur lors du traitement de le photo de profil - resize_image->imagecreatefromstring" );
		#création du container de l'image cible
		$dst = imagecreatetruecolor($newwidth, $newheight);
		if (!$dst ) throw new Exception("Erreur lors du traitement de le photo de profil - resize_image->imagecreatetruecolor" );
		#copie de l'iamge avec les nouvelles tailles
		$b=imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		if (!$b ) throw new Exception("Erreur lors du traitement de le photo de profil - resize_image->imagecopyresampled" );
		#ecriure sur disque
		//Défintion du dossier de génération
		dol_mkdir($conf->barcode->dir_temp);
		$tmpPath = tempnam($conf->barcode->dir_temp, "profilTmp_").'.jpg';
		if (!imagejpeg ($dst, $tmpPath, 80)) throw new Exception("Erreur lors du traitement de le photo de profil - resize_image->imagejpeg" );
		#libération de la mémoire
        imagedestroy ($src);
        imagedestroy ($dst);

		return $tmpPath;
	}
}
